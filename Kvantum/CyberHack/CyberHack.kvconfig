[%General]
author=Nestor Andreu based on Tsu Jan's KvFlatTeal
comment=A dark flat theme with purple, cyan and green elements
alt_mnemonic=true
animate_states=true
attach_active_tab=true
blurring=true
button_contents_shift=true
button_icon_size=16
check_size=16
click_behavior=0
combo_as_lineedit=true
combo_menu=true
composite=true
contrast=1.00
dark_titlebar=true
dialog_button_layout=0
double_click=false
drag_from_buttons=false
fill_rubberband=false
group_toolbar_buttons=false
groupbox_top_label=false
hide_combo_checkboxes=false
inline_spin_indicators=true
intensity=1.00
joined_inactive_tabs=true
large_icon_size=32
layout_margin=4
layout_spacing=2
left_tabs=true
menu_blur_radius=0
menu_shadow_depth=6
menubar_mouse_tracking=true
merge_menubar_with_toolbar=false
no_inactiveness=false
no_window_pattern=false
opaque=kaffeine,kmplayer,subtitlecomposer,kdenlive,vlc,smplayer,smplayer2,avidemux,avidemux2_qt4,avidemux3_qt4,avidemux3_qt5,kamoso,QtCreator,VirtualBox,VirtualBoxVM,trojita,dragon,digikam,lyx
popup_blurring=true
progressbar_thickness=8
reduce_menu_opacity=0
reduce_window_opacity=0
respect_DE=true
saturation=1.00
scroll_arrows=false
scroll_min_extent=50
scroll_width=12
scrollable_menu=true
scrollbar_in_view=false
shadowless_popup=true
slider_handle_length=16
slider_handle_width=16
slider_width=8
small_icon_size=16
spin_button_width=16
splitter_width=7
spread_progressbar=true
submenu_delay=250
submenu_overlap=0
toolbar_icon_size=16
toolbutton_style=0
tooltip_blur_radius=5
tooltip_delay=-1
tooltip_shadow_depth=0
transient_groove=false
transient_scrollbar=false
translucent_windows=true
tree_branch_line=true
vertical_spin_indicators=false
x11drag=all

[GeneralColors]
alt.base.color=#00000020
base.color=#003334
button.color=#09483c
button.text.color=#00e55c
dark.color=#001214
disabled.text.color=#83191b
highlight.color=#006D67
highlight.text.color=#4effff
inactive.highlight.color=#006D67
light.color=#00565f
link.color=#70b5ff
link.visited.color=#8952ff
mid.color=#001c1f
mid.light.color=#003d43
text.color=#0be5ec
tooltip.base.color=#0b0e3b
tooltip.text.color=#538fff
window.color=#002327
window.text.color=#0be5ec

[Hacks]
respect_darkness=true
blur_translucent=true
transparent_dolphin_view=true
transparent_ktitle_label=true
transparent_menutitle=true
kcapacitybar_as_progressbar=true
disabled_icon_opacity=100
force_size_grip=true
iconless_menu=false
iconless_pushbutton=false
normal_default_pushbutton=false
single_top_toolbar=false
tint_on_mouseover=0
transparent_pcmanfm_sidepane=true
lxqtmainmenu_iconsize=0
no_selection_tint=false
transparent_pcmanfm_view=true
blur_only_active_window=false
centered_forms=false
kinetic_scrolling=false
middle_click_scroll=false
noninteger_translucency=false
style_vertical_toolbars=false

[PanelButtonCommand]
frame=true
frame.element=button
frame.top=3
frame.bottom=3
frame.left=3
frame.right=3
interior=true
interior.element=button
indicator.size=9
text.normal.color=#00e55c
text.focus.color=#00ff66
text.press.color=#00ff66
text.toggle.color=#45ff93
text.disabled.color=#831f5b
text.shadow=0
text.margin=1
text.iconspacing=3
indicator.element=arrow
text.margin.top=3
text.margin.bottom=3
text.margin.left=3
text.margin.right=3
text.shadow.xshift=2
text.shadow.yshift=2
text.shadow.color=#000000
text.shadow.alpha=255
text.shadow.depth=1

[PanelButtonTool]
inherits=PanelButtonCommand
interior.element=tbutton
frame.element=tbutton

[Dock]
inherits=PanelButtonCommand
frame=true
interior=true

[DockTitle]
inherits=PanelButtonCommand
frame=false
interior=false
text.focus.color=white
text.bold=true

[IndicatorSpinBox]
inherits=PanelButtonCommand
indicator.element=arrow
indicator.size=9

[RadioButton]
inherits=PanelButtonCommand
frame=false
interior.element=radio

[CheckBox]
inherits=PanelButtonCommand
frame=false
interior.element=checkbox

[GenericFrame]
inherits=PanelButtonCommand
text.normal.color=#0be5ec
frame=true
interior=false
frame.element=common
interior.element=common
frame.top=4
frame.bottom=4
frame.left=4
frame.right=4

[LineEdit]
inherits=PanelButtonCommand
frame.element=lineedit
interior.element=lineedit
text.margin.left=2
text.margin.right=2
text.focus.color=#21ffde

[DropDownButton]
inherits=PanelButtonCommand
indicator.element=arrow-down

[IndicatorArrow]
indicator.element=arrow
indicator.size=9

[ToolboxTab]
inherits=PanelButtonCommand

[Tab]
inherits=PanelButtonCommand
interior.element=tab
text.margin.left=6
text.margin.right=6
text.margin.top=3
text.margin.bottom=3
frame.element=tab
indicator.element=tab
frame.top=4
frame.bottom=4
frame.left=4
frame.right=4
text.normal.color=#08acaf
text.press.color=#3af5ff
text.focus.color=#3af5ff
text.toggle.color=#3af5ff
text.disabled.color=#831f5b
min_width=4font
min_height=2.0font

[TabFrame]
inherits=PanelButtonCommand
frame.element=tabframe
interior=false
frame.top=4
frame.bottom=4
frame.left=4
frame.right=4

[TreeExpander]
inherits=PanelButtonCommand
frame=false
interior=false
indicator.size=9
text.normal.color=#0be5ec


[HeaderSection]
inherits=PanelButtonCommand
frame.top=2
frame.bottom=2
frame.left=2
frame.right=2
text.margin.top=2
text.margin.bottom=2

[SizeGrip]
inherits=PanelButtonCommand
frame=false
interior=false
indicator.element=resize-grip
indicator.size=13

[Toolbar]
inherits=PanelButtonCommand
indicator.element=toolbar
indicator.size=5
text.margin=0
frame.element=none
interior.element=none

[Slider]
inherits=PanelButtonCommand
frame.element=slider
interior.element=slider
frame.top=4
frame.bottom=4
frame.left=4
frame.right=4

[SliderCursor]
inherits=PanelButtonCommand
frame=false
interior.element=slidercursor

[Progressbar]
inherits=PanelButtonCommand
frame.element=progress
interior.element=progress
text.margin=0
text.focus.color=#3af5ff
text.press.color=#3af5ff
text.toggle.color=#3af5ff
frame.top=3
frame.bottom=3
frame.left=3
frame.right=3

text.bold=true
frame.expansion=0

[ProgressbarContents]
inherits=PanelButtonCommand
frame=true
frame.element=progress-pattern
interior.element=progress-pattern
frame.expansion=2.8font

[ItemView]
inherits=PanelButtonCommand
text.margin=1
text.margin.top=1
text.margin.bottom=1
text.margin.left=1
text.margin.right=1
frame.element=itemview
interior.element=itemview
frame.top=1
frame.bottom=1
frame.left=1
frame.right=1
text.normal.color=#0be5ec
text.focus.color=#3af5ff
text.press.color=#3af5ff
text.toggle.color=#3af5ff

[Splitter]
inherits=PanelButtonCommand
interior.element=splitter
frame.element=splitter
frame.top=0
frame.bottom=0
frame.left=1
frame.right=1
indicator.element=splitter-grip
indicator.size=16

[Scrollbar]
inherits=PanelButtonCommand
indicator.size=9

[ScrollbarSlider]
inherits=PanelButtonCommand
frame.element=scrollbarslider
interior.element=scrollbarslider
frame.top=6
frame.bottom=6
frame.left=6
frame.right=6

[ScrollbarGroove]
inherits=PanelButtonCommand
interior.element=scrollbargroove
frame.element=scrollbargroove
frame.top=0
frame.bottom=0
frame.left=5
frame.right=5

[MenuItem]
inherits=PanelButtonCommand
frame=true
frame.element=menuitem
interior.element=menuitem
indicator.element=menuitem
text.focus.color=#3af5ff
text.normal.color=#0be5ec
text.margin.top=1
text.margin.bottom=1
text.margin.left=3
text.margin.right=3
frame.top=2
frame.bottom=2
frame.left=2
frame.right=2

[MenuBar]
inherits=PanelButtonCommand
frame.element=none
interior.element=none

[MenuBarItem]
inherits=PanelButtonCommand
interior.element=menubaritem
frame.element=menubaritem
frame.top=2
frame.bottom=2
frame.left=2
frame.right=2
text.margin.left=4
text.margin.right=4
text.normal.color=#0be5ec

[TitleBar]
inherits=PanelButtonCommand
frame=false
interior.element=titlebar
indicator.size=12
indicator.element=mdi
text.normal.color=#14ab00
text.focus.color=#1aff00
text.bold=true
text.italic=true

[ComboBox]
inherits=PanelButtonCommand

[Menu]
inherits=PanelButtonCommand
frame.top=3
frame.bottom=3
frame.left=3
frame.right=3
frame.element=menu
interior.element=menu

[GroupBox]
inherits=GenericFrame
frame=true
frame.element=group
text.shadow=0
text.margin=0
frame.top=4
frame.bottom=4
frame.left=4
frame.right=4

[TabBarFrame]
inherits=GenericFrame
frame=false
interior=false
text.shadow=0

[ToolTip]
inherits=GenericFrame
frame.top=3
frame.bottom=3
frame.left=2
frame.right=3
interior=true
text.shadow=0
text.margin=0
interior.element=tooltip
frame.element=tooltip

[StatusBar]
inherits=GenericFrame
frame=false
interior=false

[Window]
interior=true
interior.element=window

[Focus]
frame=true
